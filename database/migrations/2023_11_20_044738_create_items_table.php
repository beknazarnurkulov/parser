<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('price');
            $table->string('size');
            $table->string('rrc');
            $table->string('artikul');
            $table->string('brand');
            $table->string('country');
            $table->text('shortDescription');
            $table->text('cel');
            $table->text('svoystva');
            $table->text('sposob');
            $table->text('sostav');
            $table->string('klaster');
            $table->string('category');
            $table->string('title');
            $table->text('images');
            $table->string('metaDescription');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('items');
    }
};
