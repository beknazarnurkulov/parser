<?php

namespace App\Console\Commands;

use App\Exports\ItemsExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ExportItemsPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:export-items';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        return Excel::store(new ItemsExport, date('d-m-Y_H:i:s_') . 'items.xlsx');
    }


}
