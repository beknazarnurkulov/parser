<?php

namespace App\Console\Commands;

use App\Jobs\ParserJob;
use App\Models\Item;
use App\Models\Product;
use App\Services\ParserService;
use DiDom\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GetUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:get-urls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $baseUrl = 'https://koreatrade.ru';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $page =  1;
        while ($page < 11) {
            $response = Http::get($this->baseUrl . '/catalog', [ 'PAGEN_1' => $page])->body();
            $document = new Document($response);
            $productLinks = $document->find('.product-thumb');
            foreach ($productLinks as $productLink) {
                $artikul = '';
                try {
                    $artikul = str_replace('Артикул: ','', $productLink->find('.element_section_thumb_table')[0]->find('tr')[0]->find('td')[1]->text());
                } catch (\Throwable $e) {
                    continue;
                }
                $this->info($artikul);
                if(Product::where('artikul', $artikul)->first() && !Item::where('artikul', $artikul)->first()) {
                    $pageUrl = $this->baseUrl . $productLink->find('a')[0]->getAttribute('href');
                    dispatch(new ParserJob($pageUrl));
                }
            }
            $page++;
            $this->info('page ' . $page);
        }
    }
}
