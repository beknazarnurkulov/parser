<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'size',
        'rrc',
        'artikul',
        'brand',
        'country',
        'shortDescription',
        'cel',
        'svoystva',
        'sposob',
        'sostav',
        'klaster',
        'category',
        'title',
        'metaDescription',
        'images'
    ];
}
