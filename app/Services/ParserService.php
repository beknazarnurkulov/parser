<?php

namespace App\Services;

use App\Models\Item;
use App\Models\Product;
use DiDom\Document;
use Illuminate\Support\Facades\Http;

class ParserService
{
    protected string $url;
    protected $page;
    protected $product;

    protected $item;

    public function __construct($url)
    {
        $this->url = $url;
        $this->setPage();
        $this->setProduct();
        $this->setItem();
    }

    public function saveData()
    {
        if(!$this->product) {
            return false;
        }

        if($this->item) {
            return false;
        }

        dd($this->page);
        $data = [
            'cel' => $this->getCel(),
            'name' => $this->getName(),
            'price' => $this->getPrice(),
            'size' => $this->getSize(),
            'rrc' => $this->getRRC(),
            'artikul' => $this->getArtikul(),
            'brand' => $this->getBrand(),
            'country' => $this->getCountry(),
            'images' => $this->getImages(),
            'klaster' => $this->getKlaster(),
            'category' => $this->getCategory(),
            'title' => $this->getTitle(),
            'sostav' => $this->getSostav(),
            'shortDescription' => $this->getShortDescription(),
            'svoystva' => $this->getSvoystva(),
            'sposob' => $this->getSposob(),
            'metaDescription' => $this->getMetaDescription(),
        ];

        Item::create($data);
    }

    public function getImages()
    {
        $list = $this->page->find('.swiper-wrapper');
        $images = '';

        foreach ($list as $item) {
            $images .= '#' . $item->find('img')[0]->getAttribute('src');
        }

        return $images;
    }

    protected function setPage()
    {
        $response = Http::get($this->url)->body();
        $this->page = new Document($response);
    }

    protected function setProduct()
    {
        $this->product = Product::where('artikul', $this->getArtikul())->first();
    }

    protected function setItem()
    {
        $this->item = Item::where('artikul', $this->getArtikul())->first();
    }

    public function getName()
    {
        return $this->product->name;
    }

    public function getPrice()
    {
        return $this->product->price;
    }

    public function getSize()
    {
        $explodedName = explode(',',$this->product->name);

        return $explodedName[count($explodedName)-1];
    }

    public function getRRC()
    {
        return $this->product->rrc;
    }

    public function getArtikul()
    {
       return str_replace('Артикул: ', '', $this->page->find('.product_attr-title')[0]->text());
    }

    public function getBrand()
    {
        return $this->page->find('.product_attr')[1]->find('a')[0]->text();
    }

    public function getCountry()
    {
        return str_replace('Страна: ', '', $this->page->find('.product_attr-title')[2]->text());
    }

    public function getShortDescription()
    {
       $oldDescription = $this->page->find('.product_field-value')[0]->text();

       return $oldDescription;
    }

    public function getCel()
    {
        $list = $this->page->find('.product_characteristics_right_html')[0]->find('li');
        $characteristics = '';

        foreach ($list as $item) {
            $characteristics .= '#' . $item->text();
        }

        return $characteristics;
    }

    public function getSvoystva()
    {
        $list = $this->page->find('.product_characteristics_right_html')[1]->find('li');
        $characteristics = '';

        foreach ($list as $item) {
            $characteristics .= '#' . $item->text();
        }

        return $characteristics;
    }

    public function getSposob()
    {
        return $this->page->find('.product_characteristics_right_html')[2]->text();
    }

    public function getSostav()
    {
        return $this->page->find('.product_characteristics_right_html')[3]->text();
    }

    public function getKlaster()
    {
        return $this->page->find('.breadcrumbs > span')[2]->text();
    }

    public function getCategory()
    {
        return $this->page->find('.breadcrumbs > span')[3]->text();
    }

    public function getTitle()
    {
        return $this->getName();
    }

    public function getMetaDescription()
    {
        return $this->page->find('.product_field-value')[0]->text();
    }
}
