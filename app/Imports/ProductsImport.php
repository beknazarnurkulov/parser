<?php

namespace App\Imports;

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (!empty($row['artikul'])) {
            return new Product([
                'name'     => $row['nomenklatura'],
                'artikul'  => $row['artikul'],
                'price'    => $row['optovaia_cena'],
                'rrc'      => $row['roznicnaia_cena'],
            ]);
        }
    }
}
